/* DO NOT EDIT.
 * Generated by ../../bin/vtkEncodeString-7.1
 * 
 * Declare the vtkSobelGradientMagnitudePass2FS string.
 *
 */
#ifndef __vtkSobelGradientMagnitudePass2FS_h
#define __vtkSobelGradientMagnitudePass2FS_h

#include "vtkRenderingOpenGL2Module.h"

VTKRENDERINGOPENGL2_EXPORT extern const char *vtkSobelGradientMagnitudePass2FS;

#endif /* #ifndef __vtkSobelGradientMagnitudePass2FS_h */
