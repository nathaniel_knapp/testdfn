dfnworks: dfnWorks python wrapper
=====================================

DFNWORKS 
################

.. automodule:: modules.dfnworks
  :members:

legal module
##############
.. automodule:: modules.legal
  :members:

generator module
###################

.. automodule:: modules.generator
  :members:

flow module
##############

.. automodule:: modules.flow
    :members:

transport module
##################

.. automodule:: modules.transport
    :members:

dfntools module
###################

.. automodule:: modules.dfntools
    :members:

distrbutions module 
####################

.. automodule:: modules.distributions
    :members:

helper module
#####################

.. automodule:: modules.helper
    :members:

generator input processing module
###################################

.. automodule:: modules.gen_input
    :members:

generator output report module
################################

.. automodule:: modules.gen_output
    :members:

LaGriT scripts module
######################

.. automodule:: modules.lagrit_scripts
    :members:

Mesh DFN module
####################
.. automodule:: modules.meshdfn
    :members:

Mesh DFN helper module
#######################

.. automodule:: modules.mesh_dfn_helper
    :members:

Parallel meshing module
########################

.. automodule:: modules.run_meshing
    :members:


